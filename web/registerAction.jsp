<%-- 
    Document   : registerAction
    Created on : 26/08/2017, 7:50:42 PM
    Author     : John-Uni-Laptop
--%>


<%@page import="java.text.*"%>
<%@page import="java.util.*"%>
<%@page import="sep.project.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Action</title>
    </head>
    <%
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String firstName = request.getParameter("firstName");
        String surname = request.getParameter("surname");
        String contact = request.getParameter("contactNumber");
        String dayDOB = request.getParameter("dayDOB");
        String monthDOB = request.getParameter("monthDOB");
        String yearDOB = request.getParameter("yearDOB");

        if (request.getParameter("tos") == null) {
    %>
    redirect to back to register.jsp with message tos is not checked, all information is filled in expect password fields
    <%
    } else if (!password.equals(passwordConfirm)) {
    %>
    redirect to back to register.jsp with message passwords not same, all information is filled in expect password fields
    <%
       } else if (email == null || firstName == null || surname == null || password == null || passwordConfirm == null || contact == null || dayDOB == null || monthDOB == null || yearDOB == null){
    %>
    redirect to back to register.jsp with message information missing, all information is filled in expect password fields
    <%
            } 
//else if (email is in database){
    %>
    redirect to back to register.jsp with message email in use, all information is filled in expect password fields
    <%
        //}
        else {
        %>
    <body>
        stores information into db starts session, message saying account created
        <%
            int id = 10; //will gen next number in db, currently a placeholder
            String DOB = dayDOB + "/" + monthDOB + "/" + yearDOB;
            String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            Customer customer = new Customer(id, firstName, surname, email, password, DOB, contact, date);
            session.setAttribute("customer", customer);
            }
        %>
    </body>
</html>
