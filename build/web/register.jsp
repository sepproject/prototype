<%-- 
    Document   : register
    Created on : 26/08/2017, 5:11:10 PM
    Author     : John-Uni-Laptop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String[] months = {"January", "Febuary", "March", "April", "May",
            "June", "July", "August", "September", "October", "November", "December"};
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
    </head>
    <body>
        <h1>Computer Creators</h1>
        <h2>Account registration</h2>
        <form action="registerAction.jsp" method="post">
            <table>
                <tr>
                    <td>Login Details</td>
                    <td>Account Details</td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td>First Name:</td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="email">
                    </td>
                    <td>
                        <input type="text" name="firstName">
                    </td>
                </tr>
                <tr>
                    <td>Password:</td>
                    <td>Surname:</td>
                </tr>
                <tr>
                    <td>
                        <input type="password" name="password">
                    </td>
                    <td>
                        <input type="text" name="surname">
                    </td>
                </tr>
                <tr>
                    <td>Re-enter Password:</td>
                    <td>Contact Number:</td>
                </tr>
                <tr>
                    <td>
                        <input type="password" name="passwordConfirm">
                    </td>
                    <td>
                        <input type="text" name="contactNumber">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        Date of Birth:
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <select name="dayDOB">
                                        <option>Day</option>
                                        <%
                                            for (int i = 1; i <= 31; i++) {
                                        %>
                                        <option value="<%= i%>"><%= i%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </td>
                                <td>
                                    <select name="monthDOB">
                                        <option>Month</option>
                                        <%
                                            for (int i = 0; i <= months.length - 1; i++) {
                                        %>
                                        <option value="<%= months[i]%>"><%= months[i]%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </td>
                                <td>
                                    <select name="yearDOB">
                                        <option>Year</option>
                                        <%
                                            for (int i = 2017; i >= 1900; i--) {
                                        %>
                                        <option value="<%= i%>"><%= i%></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p>
                <input type="checkbox" name="tos"> By signing up, you agree to our <a href="terms.jsp" target="_blank">Terms and Conditions</a>
            </p>
            <p>
                <input type="submit" value="Register">
            </p>
        </form>
    </body>
</html>