<%-- 
    Document   : index
    Created on : 26/08/2017, 3:00:53 PM
    Author     : John-Uni-Laptop
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" import="sep.project.*"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Computer Creators</title>
    </head>
    <body>
        <h1>Computer Creators</h1>
        <%
            if ((Customer) session.getAttribute("customer") != null) {
                Customer customer = (Customer) session.getAttribute("customer");
        %>
        <p>You are logged in as <%= customer.getFirstName()%></p>
        <%
        } else {
        %>
        <p>Welcome to Computer Creators</p>
        <table>
            <tr>
                <td>
                    <a href="register.jsp">Register</a>
                </td>
                <td>
                    <a href="login.jsp">Login</a>
                </td>
            </tr>
        </table>
        <%
            }
        %>
    </body>
</html>
