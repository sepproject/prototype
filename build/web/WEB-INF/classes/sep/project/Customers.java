/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sep.project;

import java.util.*;

/**
 *
 * @author John-Uni-Laptop
 */
public class Customers {
    private ArrayList<Customer> list = new ArrayList<Customer>();

    public ArrayList<Customer> getList() {
        return list;
    }
    
    public void addCustomer(Customer customer){
        list.add(customer);
    }
    
    public void removeCustomer(Customer customer){
        list.remove(customer);
    }
    
    public Customer login(String email, String password){
        for (Customer customer : list)
            if (customer.getEmail().equals(email) && customer.getPassword().equals(password))
                return customer;
        return null;
    }
    
    public Boolean isRegistered(String email){
        for (Customer customer : list)
            if (customer.getEmail().equals(email))
                return true;
        return false;
    }
}
